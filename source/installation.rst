.. _installation:

Установка
=========

When we refer to "Python 3" in this book, we will be referring to any version 
of Python equal to or greater than version `Python 3.6.0 <https://www.python.org/downloads/>`_.

Установка в Windows
-------------------

Посетите страницу http://www.python.org/download/ и загрузите последнюю версию. At the time of this writing, it was Python 3.6.0
Установка производится так же, как и для любых других программ для Windows.

Note that if your Windows version is pre-Vista, you should [download Python 3.4 only](https://www.python.org/downloads/windows/) as later versions require newer versions of Windows.

CAUTION: Make sure you check option `Add Python 3.6 to PATH`.

To change install location, click on `Customize installation`, then `Next` and enter `C:\python36` (or another appropriate location) as the install location.

If you didn't check the `Add Python 3.6 PATH` option earlier, check `Add Python to environment variables`. This does the same thing as `Add Python 3.6 to PATH` on the first install screen.

You can choose to install Launcher for all users or not, it does not matter much. Launcher is used to switch between different versions of Python installed.

If your path was not set correctly (by checking the `Add Python 3.6 Path` or `Add Python to environment variables` options), then follow the steps in the next section (`DOS Prompt`) to fix it. Otherwise, go to the `Running Python prompt on Windows` section in this document.

NOTE: For people who already know programming, if you are familiar with Docker, check out [Python in Docker](https://hub.docker.com/_/python/) and [Docker on Windows](https://docs.docker.com/windows/).

### DOS Prompt {#dos-prompt}

Для использования Python из командной строки Windows, т.е. приглашения DOS, 
необходимо установить должным образом переменную PATH.

Для Windows 2000, XP и 2003 перейдите в "Панель управления" --> "Система" 
--> "Дополнительно" --> "Переменные среды". Нажмите на переменную с именем 
``PATH`` в отделе "Системные переменные", после этого выберите 
"Редактировать" и допишите ``;C:\Python36`` к концу того, что там уже есть 
(проверьте, существует ли такой каталог, так как для более новых версий Python 
он будет иметь другое имя). Конечно, укажите действительное имя каталога.
                
Для более старых версий Windows добавьте следующую строку в файл 
``C:\AUTOEXEC.BAT`` : '``PATH=%PATH%;C:\Python36``' (без кавычек) и 
перезапустите систему. Для Windows NT используйте файл ``AUTOEXEC.NT``.

Для Windows Vista:

1. Нажмите кнопку "Пуск" и выберите "Панель управления".
2. Нажмите "Система", справа вы увидите "Просмотр основных сведений о вашем 
   компьютере". Слева -- список действий, последним из которых будет 
   "Дополнительные параметры системы." Нажмите её. Отобразится вкладка 
   "Дополнительно" диалога параметров системы. Нажмите кнопку "Переменные 
   среды" справа внизу.
3. В нижнем поле под названием "Системные переменные" прокрутите до ``Path`` и 
   нажмите кнопку "Редактировать".
4. Измените путь, как нужно.
5. Перезапустите систему. Vista не обновляет системные пути до перезагрузки.


Для Windows 7 и 8:

1. Щёлкните правой кнопкой мыши на значке "Компьютер" на рабочем столе и 
   выберите "Свойства"; иначе -- нажмите кнопку "Пуск" и выберите "Панель 
   Управления" --> "Система и безопасность" --> "Система". Нажмите 
   "Дополнительные параметры системы" слева, а затем выберите вкладку 
   "Дополнительно". Внизу нажмите кнопку "Переменные среды" и в отделе
   "Системные переменные" найдите переменную ``PATH``, выберите её и нажмите
   "Редактировать".
2. Перейдите к концу строки в поле "Значение переменной" и допишите 
   ``;C:\Python36``.
3. Если значение переменной было ``%SystemRoot%\system32;``, теперь оно примет
   вид ``%SystemRoot%\system32;C:\Python36``
4. Нажмите "Ok", и всё. Перезагрузка не требуется, however you may have to close and reopen the command line.

For Windows 10:

Windows Start Menu > `Settings` > `About` > `System Info` (this is all the way over to the right) > `Advanced System Settings` > `Environment Variables` (this is towards the bottom) > (then highlight `Path` variable and click `Edit`) > `New` > (type in whatever your python location is.  For example, `C:\Python35\`)

Запуск командной строки Python в Windows
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Если вы должным образом :ref:`установили значение переменной PATH <installation-dos-prompt>`,
теперь можно запускать интерпретатор из командной строки.

Чтобы открыть терминал в Windows, нажмите кнопку "Пуск" и выберите "Выполнить".
В появившемся диалоговом окне наберите ``cmd`` и нажмите ``Enter``.

Затем наберите ``python`` и проверьте, нет ли ошибок.

Для пользователей Mac OS X
--------------------------

For Mac OS X users, use `Homebrew <http://brew.sh>`_: 

::

  brew install python3

To verify, open the terminal by pressing `[Command + Space]` keys (to open Spotlight search), type `Terminal` and press `[enter]` key. Now, run `python3` and ensure there are no errors.

Установка в GNU/Linux и BSD
---------------------------

For GNU/Linux users, use your distribution's package manager to install Python 3, e.g. on Debian & Ubuntu: `sudo apt-get update && sudo apt-get install python3`.

To verify, open the terminal by opening the `Terminal` application or by pressing `Alt + F2` and entering `gnome-terminal`. If that doesn't work, please refer the documentation of your particular GNU/Linux distribution. Now, run `python3` and ensure there are no errors.

You can see the version of Python on the screen by running:

::

$ python3 -V
Python 3.6.0

.. note::
    ``$`` -- это приглашение командной строки.  Оно может выглядеть по-разному 
    в зависимости от настроек вашей ОС, поэтому я буду обозначать приглашение 
    просто одним символом ``$``.

CAUTION: Output may be different on your computer, depending on the version of Python software installed on your computer.

Резюме
------

С этого момента мы будем считать, что Python в вашей системе установлен.

Далее мы приступим к написанию нашей первой программы на Python.
